import interfaces as controller_template
from itertools import product
from typing import Tuple, List


def print_QTable(dic):
    print('QTable--------------------------------------------------')
    for key, value in dic.items():
        print('State = ', end=str(key[0]))
        print(' , Action = ', end=str(key[1]))
        print(' -> ', end=str(value))
        print()
    print('--------------------------------------------------------')   


class State(controller_template.State):
    def __init__(self, sensors: list):
        self.sensors = sensors


    def compute_features(self) -> Tuple:
        """
        This function should take the raw sensor information of the car (see below) and compute useful features for selecting an action
        The car has the following sensors:

        self.sensors contains (in order):
            0 track_distance_left: 1-100
            1 track_distance_center: 1-100
            2 track_distance_right: 1-100
            3 on_track: 0 if off track, 1 if on normal track, 2 if on ice
            4 checkpoint_distance: 0-???
            5 car_velocity: 10-200
            6 enemy_distance: -1 or 0-???
            7 enemy_position_angle: -180 to 180
            8 enemy_detected: 0 or 1
            9 checkpoint: 0 or 1
           10 incoming_track: 1 if normal track, 2 if ice track or 0 if car is off track
           11 bomb_distance = -1 or 0-???
           12 bomb_position_angle = -180 to 180
           13 bomb_detected = 0 or 1
          (see the specification file/manual for more details)
        :return: A Tuple containing the features you defined
        """
        dist_left = self.sensors[0]
        dist_front = self.sensors[1]
        dist_right = self.sensors[2]       
        on_track = self.sensors[3]

        return (dist_left, dist_front, dist_right, on_track)


    def discretize_features(self, features: Tuple) -> Tuple:
        """
        This function should map the (possibly continuous) features (calculated by compute features) and discretize them.
        :param features 
        :return: A tuple containing the discretized features
        """
        dist_left_des = int(features[0]/10)
        dist_front_des = int(features[1]/10)
        dist_right_des = int(features[2]/10)
        on_track = features[3]

        discretized_features = (dist_left_des, dist_front_des, dist_right_des, on_track)
        return discretized_features


    @staticmethod
    def discretization_levels() -> Tuple:
        """
        This function should return a vector specifying how many discretization levels to use for each state feature.
        :return: A tuple containing the discretization levels of each feature
        """
        return (11, 11, 11, 3)


    @staticmethod
    def enumerate_all_possible_states() -> List:
        """
        Handy function that generates a list with all possible states of the system.
        :return: List with all possible states
        """
        levels = State.discretization_levels()
        levels_possibilities = [(j for j in range(i)) for i in levels]
        return [i for i in product(*levels_possibilities)]


class QTable(controller_template.QTable):
    def __init__(self, is_load=False, QTable_from_file=None):
        """
        This class is used to create/load/store your Q-table. To store values we strongly recommend the use of a Python
        dictionary.
        """
        if is_load:
            self.QTable = QTable_from_file
        else:
            self.QTable = {}
            list_of_possible_states = State.enumerate_all_possible_states()
            for state in list_of_possible_states:
                for action in range(1,6):
                    self.QTable[(state, action)] = 0
        #print_QTable(self.QTable)


    def get_q_value(self, key: State, action: int) -> float:
        """
        Used to securely access the values within this q-table
        :param key: a State object 
        :param action: an action
        :return: The Q-value associated with the given state/action pair
        """
        key = (key.discretize_features(key.compute_features()))
        return self.QTable[(key, action)]


    def set_q_value(self, key: State, action: int, new_q_value: float) -> None:
        """
        Used to securely set the values within this q-table
        :param key: a State object 
        :param action: an action
        :param new_q_value: the new Q-value to associate with the specified state/action pair
        :return: 
        """
        key = (key.discretize_features(key.compute_features()))
        self.QTable[(key, action)] = new_q_value


    @staticmethod
    def load(path: str) -> "QTable":
        """
        This method should load a Q-table from the specified file and return a corresponding QTable object
        :param path: path to file
        :return: a QTable object
        """
        QTable_in_file = open(path ,"r").read()
        QTable_from_file = eval(QTable_in_file)
        return QTable(is_load=True, QTable_from_file=QTable_from_file)

 
    def save(self, path: str, *args) -> None:
        """
        This method must save this QTable to disk in the file file specified by 'path'
        :param path: 
        :param args: Any optional args you may find relevant; beware that they are optional and the function must work
                     properly without them.
        """
        file_to_write_QTable = open(path, 'w+')
        file_to_write_QTable.write(str(self.QTable))
        file_to_write_QTable.close()


class Controller(controller_template.Controller):
    def __init__(self, q_table_path: str):
        if q_table_path is None:
            self.q_table = QTable()
        else:
            self.q_table = QTable.load(q_table_path)


    def update_q(self, new_state: State, old_state: State, action: int, reward: float, end_of_race: bool) -> None:
        """
        This method is called by the learn() method in simulator.Simulation() to update your Q-table after each action is taken
        :param new_state: The state the car just entered
        :param old_state: The state the car just left
        :param action: the action the car performed to get to new_state
        :param reward: the reward the car received for getting to new_state  
        :param end_of_race: boolean indicating if a race timeout was reached
        """
        if end_of_race == False:
            a = 0.5
            y = 0.95
            list_of_actions = []
            for act in range(1,6):
                list_of_actions.append(self.q_table.get_q_value(new_state, act))

            max_action_Q = max(list_of_actions)
            new_Q_value = (1 - a)*self.q_table.get_q_value(old_state, action) + a * (reward + y * max_action_Q)

            self.q_table.set_q_value(old_state, action, new_Q_value)


    def compute_reward(self, new_state: State, old_state: State, action: int, n_steps: int,
                       end_of_race: bool) -> float:
        """
        This method is called by the learn() method in simulator.Simulation() to calculate the reward to be given to the agent
        :param new_state: The state the car just entered
        :param old_state: The state the car just left
        :param action: the action the car performed to get in new_state
        :param n_steps: number of steps the car has taken so far in the current race
        :param end_of_race: boolean indicating if a race timeout was reached
        :return: The reward to be given to the agent
        """
        nw_st_fts = (new_state.discretize_features(new_state.compute_features()))
        old_st_fts = (old_state.discretize_features(old_state.compute_features()))

        dist_left_old_st = old_st_fts[0]
        dist_left_new_st = nw_st_fts[0]

        dist_front_old_st = old_st_fts[1]
        dist_front_new_st = nw_st_fts[1]

        dist_right_old_st = old_st_fts[2]
        dist_right_new_st = nw_st_fts[2]      

        on_track_old_st = old_st_fts[3]
        on_track_new_st = nw_st_fts[3]

        
        if dist_front_new_st >= 8:
            straight_in_track = dist_front_new_st*2    
        elif dist_front_new_st <= 2:
            straight_in_track = -10
        else:
            straight_in_track = 0

        if dist_front_old_st < dist_front_new_st:
            if dist_left_new_st < dist_right_new_st and action == 2:
                turn_reward = 10
            elif dist_left_new_st > dist_right_new_st and action == 1:
                turn_reward = 10
            else:
                turn_reward = -2
        else:
            turn_reward = -2


        if on_track_new_st == 0:
            off_track = -1
        else:
            off_track = 0


        reward = -0.05 + turn_reward + off_track + straight_in_track
        return reward


    def take_action(self, new_state: State, episode_number: int) -> int:
        """
        Decides which action the car must execute based on its Q-Table and on its exploration policy
        :param new_state: The current state of the car 
        :param episode_number: current episode/race during the training period
        :return: The action the car chooses to execute
        """
        import random
        e = 0.75 + (episode_number/400)
        if e >= random.uniform(0,1):
            list_of_actions = [self.q_table.get_q_value(new_state, action) for action in range(1,6)]
            action = 1 + list_of_actions.index(max(list_of_actions))
        else:
            action = random.randint(1,5)

        return action